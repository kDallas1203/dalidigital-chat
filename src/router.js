import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'
import Chat from './views/Chat.vue'

Vue.use(Router)

export default new Router({
	mode: 'history',
	routes: [
		{
			path: '/',
			name: 'home',
			component: Home
		},
		{
			path: '/auth',
			name: 'auth',
			component: Home
		},
		{
			path: '/chat',
			name: 'chat',
			component: Chat,
			props: true,
			beforeEnter: (to, from, next) => {
				if(to.params.username) {
					next();
				} else {
					next({ name: 'auth'})
				}

			}
		},
	]
})
